# ‘Libre Hybridation’ workshop @ EnsAD Paris

![](img/EnsAD-MRC-img01.gif)


Results of a three-hour workshop given by [Bonjour Monde](http://www.bonjourmonde.net) at [École des Arts Décoratifs](http://www.ensad.fr) (Paris, France) on 15 October 2018, part of the ‘Libre hybridation’ seminar, organised by [Julie Blanc](http://www.julie-blanc.fr), [Lucile Haute](http://www.ensadlab.fr/lucile-haute/#) et [Quentin Juhel](http://www.juhel-quentin.fr).

The students used [DataFace](https://gitlab.com/bonjour-monde/tools/dataface), a program based on the library [FontTools](https://pypi.org/project/FontTools/), to present new variations on the open-source typeface [Syne](https://gitlab.com/bonjour-monde/fonderie/syne-typeface). Four of these explorations are used on the online-tester as extreme masters, giving life to a rather experimental [Variable Font](https://medium.com/variable-fonts/https-medium-com-tiro-introducing-opentype-variable-fonts-12ba6cd2369).

[Some results](http://51.254.121.46/_ressources/mrc-variable/)


![](img/EnsAD-MRC-img02.JPG)
![](img/EnsAD-MRC-img03.JPG)